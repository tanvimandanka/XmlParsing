package com.example.tanvi.xmlparsing

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.tanvi.xmlparsing.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity() {

    lateinit var mActivityMainBinding : ActivityMainBinding
    lateinit var mMainActivitModel : MainActivityModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mMainActivitModel = MainActivityModel(this@MainActivity)
        mActivityMainBinding.mMainActivityModel = mMainActivitModel

    }

}
