package com.example.tanvi.xmlparsing

import android.content.ContentValues.TAG
import android.content.Context
import android.databinding.ObservableField
import android.view.View
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.util.Log
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import org.w3c.dom.Element
import org.w3c.dom.Node
import java.lang.reflect.Array.getLength
import javax.xml.parsers.DocumentBuilderFactory
import org.xml.sax.InputSource
import javax.xml.parsers.SAXParserFactory
import android.os.AsyncTask







/**
 * Created by tanvi on 21/3/18.
 */

class MainActivityModel : Observable {
    var context: Context
    var text: ObservableField<String> = ObservableField()
    private val NAMESPACE = "https://api.authorize.net/soap/v1/"
    private val URL = "https://apitest.authorize.net/soap/v1/Service.asmx?wsdl"
    private val SOAP_ACTION = "https://api.authorize.net/soap/v1/AuthenticateTest"
    private val METHOD_NAME = "AuthenticateTest"

    constructor(context: Context) {
        this.context = context
        text.set("")
    }

    fun onXmlPullParserButtonClicked(view: View) {
        try {
            var xmlPullParserFactory = XmlPullParserFactory.newInstance() as XmlPullParserFactory
            var xmlPullParser = xmlPullParserFactory.newPullParser()

            var inputStream = context.assets.open("sample.xml") as InputStream
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            xmlPullParser.setInput(inputStream, null)

            var countries = parseXML(xmlPullParser) as List<Country>

            var text = ""

            for (i in 0..countries.size - 1) {
                text += "id :" + countries.get(i).id + " name :" + countries.get(i).name + " Capital : " + countries.get(i).capital + "\n"
            }

            this.text.set(text)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * xml parsing using Dom
     */
    fun onDomParserButtonClicked(view: View) {
        var countries = ArrayList<Country>()
        var inputStream = context.assets.open("sample.xml") as InputStream
        val dbFactory = DocumentBuilderFactory.newInstance()
        val dBuilder = dbFactory.newDocumentBuilder()
        val doc = dBuilder.parse(inputStream)

        val element = doc.getDocumentElement()
        element.normalize()

        val nList = doc.getElementsByTagName("country")
        var text = ""
        for (i in 0 until nList.getLength()) {

            val node = nList.item(i)
            if (node.getNodeType() === Node.ELEMENT_NODE) {
                val element2 = node as Element
                text += "id :" + element2.getAttribute("id") + " name :" + getValue("name", element2) + " Capital : " + getValue("capital", element2) + "\n"
            }
        }//end of for loop

        this.text.set(text)
    }

    private fun getValue(tag: String, element: Element): String {
        val nodeList = element.getElementsByTagName(tag).item(0).childNodes
        val node = nodeList.item(0) as Node
        return node.nodeValue
    }

    /**
     * parse xml using xmlPullParser
     */
    private fun parseXML(xmlPullParser: XmlPullParser): Any {
        var countries = ArrayList<Country>()
        var eventType = xmlPullParser.eventType
        var country: Country? = null

        while (eventType != XmlPullParser.END_DOCUMENT) {
            var name = ""
            when (eventType) {
                XmlPullParser.START_DOCUMENT -> {
                    countries = ArrayList<Country>()
                }
                XmlPullParser.START_TAG -> {
                    name = xmlPullParser.name
                    if (name.equals("country")) {
                        country = Country()
                        country.id = xmlPullParser.getAttributeValue(null, "id")
                    } else if (country != null) {
                        if (name.equals("name")) {
                            country.name = xmlPullParser.nextText()
                        } else if (name.equals("capital")) {
                            country.capital = xmlPullParser.nextText()
                        }
                    }
                }
                XmlPullParser.END_TAG -> {
                    name = xmlPullParser.name
                    if (name.equals("country") && country != null) {
                        countries.add(country)
                    }
                }

            }
            eventType = xmlPullParser.next()
        }
        return countries
    }

    fun onSaxParseButtonClicked(view: View){
        var inputStream = context.assets.open("sample.xml") as InputStream
        var countries = parse(inputStream) as List<Country>

        var text = ""

        for (i in 0..countries.size - 1) {
            text += "id :" + countries.get(i).id + " name :" + countries.get(i).name + " Capital : " + countries.get(i).capital + "\n"
        }

        this.text.set(text)
    }

    /**
     * parse using SAX Parser
     */
    fun parse(inputStream: InputStream): List<Country>? {
        var countries: List<Country>? = null
        try {
            // create a XMLReader from SAXParser
            val xmlReader = SAXParserFactory.newInstance().newSAXParser()
                    .getXMLReader()
            // create a SAXXMLHandler
            val saxHandler = SAXXMLHandler()
            // store handler in XMLReader
            xmlReader.setContentHandler(saxHandler)
            // the process starts
            xmlReader.parse(InputSource(inputStream))
            // get the `Employee list`
            countries = saxHandler.countryList

        } catch (ex: Exception) {
            Log.d("XML", "SAXXMLParser: parse() failed")
        }

        // return Employee list
        return countries
    }


    fun soap(view: View){

        val thread = Thread(Runnable {
            try {
                val request = SoapObject(NAMESPACE, METHOD_NAME)
                request.addProperty("name", "44vmMAYrhjfhj66fhJN")
                request.addProperty("transactionKey", "9MDQ7fghjghjh53H48k7e7n")
                val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
                envelope.setOutputSoapObject(request)
                val androidHttpTransport = HttpTransportSE(URL)
                try {
                    androidHttpTransport.call(SOAP_ACTION, envelope)

                    //SoapPrimitive  resultsRequestSOAP = (SoapPrimitive) envelope.getResponse();
                    // SoapPrimitive  resultsRequestSOAP = (SoapPrimitive) envelope.getResponse();
                    val resultsRequestSOAP = envelope.bodyIn as SoapObject


                    text.set(resultsRequestSOAP.toString())
                    System.out.println("Response::" + resultsRequestSOAP.toString())

                } catch (e: Exception) {
                    println("Error" + e)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        thread.start()

    }
}
