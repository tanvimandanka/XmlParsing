package com.example.tanvi.xmlparsing

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler

/**
 * Created by tanvi on 21/3/18.
 */
class SAXXMLHandler : DefaultHandler() {
    var countryList = ArrayList<Country>()
    var country = Country()
    var tempVal: String = ""
    // Event Handlers
    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String,
                              attributes: Attributes) {
        // reset
        tempVal = ""
        if (qName.equals("country", ignoreCase = true)) {
            // create a new instance of employee
            country = Country()
            country.id = attributes.getValue("id").toString()
        }
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray, start: Int, length: Int) {
        tempVal = String(ch, start, length)
    }

    @Throws(SAXException::class)
    override fun endElement(uri: String, localName: String, qName: String) {
        if (qName.equals("country", ignoreCase = true)) {
            // add it to the list
            countryList.add(country)
        } else if (qName.equals("name", ignoreCase = true)) {
            country.name = tempVal
        } else if (qName.equals("capital", ignoreCase = true)) {
            country.capital = tempVal
        }
    }
}